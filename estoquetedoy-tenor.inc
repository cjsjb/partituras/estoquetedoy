\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key c \major

		R1*4  |
%% 5
		r4 g 8 g g g 4 g 8 ~  |
		g 2. r8 g  |
		f 8 f 4 g 4. a 4  |
		g 1  |
		r4 f 8 f f 4 a  |
%% 10
		g 4 c d c  |
		b, 4. g 8 ~ g 2  |
		g 2 g 8 g 4 a 8 ~  |
		a 1  |
		r4 r8 e e f 4 g 8 ~  |
%% 15
		g 1  |
		r4 r8 d d e 4 f 8 ~  |
		f 2. r4  |
		d 4 c b, d  |
		c 1  |
%% 20
		R1  |
		r4 g 8 g g g 4 g 8 ~  |
		g 2. r8 g  |
		f 8 f 4 g 4. a 4  |
		g 1  |
%% 25
		r4 f 8 f f 4 a  |
		g 4 c d c  |
		b, 4. ( g 8 ~ g 2 )  |
		g 2 g 8 g 4 a 8 ~  |
		a 1  |
%% 30
		r4 r8 e e f 4 g 8 ~  |
		g 1  |
		r4 r8 d d e 4 f 8 ~  |
		f 2. r4  |
		d 4 c b, d  |
%% 35
		c 1  |
		R1*2  |
		r4 r8 g a g 4 a 8 ~  |
		a 8 g ~ g 2. ~  |
%% 40
		g 1  |
		R1  |
		r4 r8 g a g 4 a 8 ~  |
		a 8 g ~ g 2. ~  |
		g 1  |
%% 45
		r4 a a g 8 f ~  |
		f 8 f 4 f 8 g 4 a ~  |
		a 8 r b b b 4. a 8  |
		g 4 f 8 e 4 f g 8  |
		a 4 f 8 f ~ f 2  |
%% 50
		r4 r8 f e f 4 g 8 ~  |
		g 1 ~  |
		g 2 r  |
		a 4 f 8 f ~ f 2  |
		r4 f e d  |
%% 55
		e 1  |
		R1  |
		r4 g 8 g g g 4 g 8 ~  |
		g 2. r8 g  |
		f 8 f 4 g 4. a 4  |
%% 60
		g 1  |
		r4 f 8 f f 4 a  |
		g 4 c d c  |
		b, 4. g 8 ~ g 2  |
		g 2 g 8 g 4 a 8 ~  |
%% 65
		a 1  |
		r4 r8 e e f 4 g 8 ~  |
		g 1  |
		r4 r8 d d e 4 f 8 ~  |
		f 2. r4  |
%% 70
		d 4 c b, d  |
		c 1  |
		R1*2  |
		r4 r8 g a g 4 a 8 ~  |
%% 75
		a 8 g ~ g 2. ~  |
		g 1  |
		R1  |
		r4 r8 g a g 4 a 8 ~  |
		a 8 g ~ g 2. ~  |
%% 80
		g 1  |
		r4 a a g 8 f ~  |
		f 8 f 4 f 8 g 4 a ~  |
		a 8 r b b b 4. a 8  |
		g 4 f 8 e 4 f g 8  |
%% 85
		a 4 f 8 f ~ f 2  |
		r4 r8 f e f 4 g 8 ~  |
		g 1 ~  |
		g 2 r  |
		a 4 f 8 f ~ f 2  |
%% 90
		r4 f e d  |
		e 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Es -- to que te doy es vi -- "no y" pan, Se -- ñor;
		es -- to que te doy es mi tra -- ba -- jo... __
		Pa pa pa pa, __
		pa pa pa pa, __
		pa pa pa pa __
		"...de" mi ca -- mi -- nar.

		Es -- to que te doy, mi vi -- da es, Se -- ñor;
		es "mi a" -- mor, tam -- bién es mi do -- lor... __
		Pa pa pa pa, __
		pa pa pa pa, __
		pa pa pa pa __
		"...y" mi o -- ra -- ción.

		To -- ma mi vi -- da. __
		Da -- me tu ma -- no. __
		Cam -- bia mi pan __ en tu car -- ne, __
		y mi vi -- no en tu san -- gre "y a" mí, Se -- ñor, __
		re -- nué -- va -- me, __
		lím -- pia -- me __
		y sál -- va -- me.

		Es -- to que te doy, no só -- lo yo, Se -- ñor:
		es -- ta voz tam -- bién es de "mi her" -- ma -- no...
		Pa pa pa pa, __
		pa pa pa pa, __
		pa pa pa pa __
		"...en" co -- mu -- ni -- dad.

		To -- ma mi vi -- da. __
		Da -- me tu ma -- no. __
		Cam -- bia mi pan __ en tu car -- ne, __
		y mi vi -- no en tu san -- gre "y a" mí, Se -- ñor, __
		re -- nué -- va -- me, __
		lím -- pia -- me __
		y sál -- va -- me.
	}
>>
