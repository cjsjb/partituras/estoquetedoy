\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key c \major

		R1*4  |
%% 5
		r4 g' 8 g' g' g' 4 g' 8 ~  |
		g' 2. r8 g'  |
		f' 8 f' 4 g' 4. a' 4  |
		g' 1  |
		r4 f' 8 f' f' 4 a'  |
%% 10
		g' 4 c' d' c'  |
		b 4. g' 8 ~ g' 2 ~  |
		g' 1  |
		r4 f' 8 f' f' 4. e' 8  |
		d' 4 d' 8 e' 4 f' 4.  |
%% 15
		r4 g' 8 g' g' 4. f' 8  |
		e' 4 d' 8 c' 4 ( d' 8 e' 4 ) |
		r4 f' 8 f' f' 4 e'  |
		d' 4 c' b d'  |
		c' 1  |
%% 20
		R1  |
		r4 g' 8 g' g' g' 4 g' 8 ~  |
		g' 2. r8 g'  |
		f' 8 f' 4 g' 4. a' 4  |
		g' 1  |
%% 25
		r4 f' 8 f' f' 4 a'  |
		g' 4 c' d' c'  |
		b 4. ( g' 8 ~ g' 2 ~  |
		g' 1 ) |
		r4 f' 8 f' f' 4. e' 8  |
%% 30
		d' 4 d' 8 e' 4 f' 4.  |
		r4 g' 8 g' g' 4. f' 8  |
		e' 4 d' 8 c' 4 ( d' 8 e' 4 ) |
		r4 f' 8 f' f' 4 e'  |
		d' 4 c' b d'  |
%% 35
		c' 1  |
		R1  |
		g' 2 c' 4 c' 8 b ~  |
		b 8 g' ~ g' 2.  |
		r4 c' c' f'  |
%% 40
		e' 8 d' 4 d' 2 r8  |
		g' 2 c' 4 c' 8 b ~  |
		b 8 g' ~ g' 2.  |
		r4 f' g' a'  |
		g' 1  |
%% 45
		r4 f' f' e' 8 d' ~  |
		d' 8 d' 4 d' 8 e' 4 f' ~  |
		f' 8 r g' g' g' 4. f' 8  |
		e' 4 d' 8 c' 4 d' e' 8  |
		f' 4 a 8 a ~ a 2  |
%% 50
		r4 r8 d' c' d' 4 e' 8 ~  |
		e' 1 ~  |
		e' 2 r  |
		f' 4 a 8 a ~ a 2  |
		r4 c' c' b  |
%% 55
		c' 1  |
		R1  |
		r4 g' 8 g' g' g' 4 g' 8 ~  |
		g' 2. r8 g'  |
		f' 8 f' 4 g' 4. a' 4  |
%% 60
		g' 1  |
		r4 f' 8 f' f' 4 a'  |
		g' 4 c' d' c'  |
		b 4. g' 8 ~ g' 2 ~  |
		g' 1  |
%% 65
		r4 f' 8 f' f' 4. e' 8  |
		d' 4 d' 8 e' 4 f' 4.  |
		r4 g' 8 g' g' 4. f' 8  |
		e' 4 d' 8 c' 4 d' 8 e' 4  |
		r4 f' 8 f' f' 4 e'  |
%% 70
		d' 4 c' b d'  |
		c' 1  |
		R1  |
		g' 2 c' 4 c' 8 b ~  |
		b 8 g' ~ g' 2.  |
%% 75
		r4 c' c' f'  |
		e' 8 d' 4 d' 2 r8  |
		g' 2 c' 4 c' 8 b ~  |
		b 8 g' ~ g' 2.  |
		r4 f' g' a'  |
%% 80
		g' 1  |
		r4 f' f' e' 8 d' ~  |
		d' 8 d' 4 d' 8 e' 4 f' ~  |
		f' 8 r g' g' g' 4. f' 8  |
		e' 4 d' 8 c' 4 d' e' 8  |
%% 85
		f' 4 a 8 a ~ a 2  |
		r4 r8 d' c' d' 4 e' 8 ~  |
		e' 1 ~  |
		e' 2 r  |
		f' 4 a 8 a ~ a 2  |
%% 90
		r4 c' c' b  |
		c' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Es -- to que te doy es vi -- "no y" pan, Se -- ñor;
		es -- to que te doy es mi tra -- ba -- jo; __
		es mi co -- ra -- zón, mi al -- ma;
		es mi cuer -- "po y" mi ra -- zón, __
		el es -- fuer -- zo de mi ca -- mi -- nar.

		Es -- to que te doy, mi vi -- da es, Se -- ñor;
		es "mi a" -- mor, tam -- bién es mi do -- lor; __
		es la i -- lu -- sión, mis sue -- ños;
		es mi go -- "zo y" mi llo -- rar; __
		es mi can -- to y mi o -- ra -- ción.

		To -- ma mi vi -- da, __
		pon -- "la en" tu co -- ra -- zón.
		Da -- me tu ma -- no __
		y llé -- va -- me.
		Cam -- bia mi pan en tu car -- ne, __
		y mi vi -- no en tu san -- gre "y a" mí, Se -- ñor, __
		re -- nué -- va -- me, __
		lím -- pia -- me __
		y sál -- va -- me.

		Es -- to que te doy, no só -- lo yo, Se -- ñor:
		es -- ta voz tam -- bién es de "mi her" -- ma -- no; __
		es "la u" -- nión, la paz, el or -- den,
		"la ar" -- mo -- ní -- "a y" la fe -- li -- ci -- dad,
		es un can -- to en co -- mu -- ni -- dad.

		To -- ma mi vi -- da, __
		pon -- "la en" tu co -- ra -- zón.
		Da -- me tu ma -- no __
		y llé -- va -- me.
		Cam -- bia mi pan en tu car -- ne, __
		y mi vi -- no en tu san -- gre "y a" mí, Se -- ñor, __
		re -- nué -- va -- me, __
		lím -- pia -- me __
		y sál -- va -- me.
	}
>>
