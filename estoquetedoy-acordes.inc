\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		c1 e1:m f1 g1

		% esto que te doy...
		c1 e1:m f1 g1
		f1 c1 g1 
		\set chordChanges = ##f
			g2 g4 fis4
		\set chordChanges = ##t
		f1 g1 e1:m a1:m
		f1 g1 c1 g1

		% esto que te doy...
		\bar "||"
		c1 e1:m f1 g1
		f1 c1 g1 
		\set chordChanges = ##f
			 g2 g4 fis4
		\set chordChanges = ##t
		f1 g1 e1:m a1:m
		f1 g1 c1 g1

		% toma mi vida...
		\bar "||"
		c1 e1:m f1 g1
		c1 e1:m f1 g1
		f1 g1 e1:m a1:m
		f1 g1 c1 c1:7
		f1 g4
		\set chordChanges = ##f
		      g4 r2
		\set chordChanges = ##f
			    c1 g1

		% esto que te doy...
		\bar "||"
		c1 e1:m f1 g1
		f1 c1 g1 
		\set chordChanges = ##f
			 g2 g4 fis4
		\set chordChanges = ##t
		f1 g1 e1:m a1:m
		f1 g1 c1 g1

		% toma mi vida...
		\bar "||"
		c1 e1:m f1 g1
		c1 e1:m f1 g1
		f1 g1 e1:m a1:m
		f1 g1 c1 c1:7
		f1 g4
		\set chordChanges = ##f
		      g4 r2
		\set chordChanges = ##f
			    c1
	}
